This is code from the COMP 523 class meeting on Monday, September 16, 2019. I
(instructor Jeff Terrell) discussed React.js, what it is, and why it's
important. I also gave an extended example of a React.js app to play a game of
tic-tac-toe. That code is included in this archive.

To run it, start an HTTP server in this directory (i.e. the one containing
index.html). If you don't know how to do that, see [1]. Then open up
http://localhost:8080 (or whatever port your server is listening on).

As a refresher, here are the big ideas of React.js that I mentioned in class.
React.js is:

- a very popular approach to building web-based user interfaces
- useful for building dynamic web apps (and not so much for static web pages)
- uses a unidirectional flow from state to markup, rather than the more
  complicated two-way data bindings of other frameworks
- uses user-defined "components" to manage a piece of UI at a time (each
  component returns markup that should depend on either the component's state
  or its props)
- allows components to be parameterized using "props", just like functions

Also, if you are targeting a mobile platform rather than a web platform, React
Native has a very similar approach. You can see a React Native tic-tac-toe app
at [2]. Perhaps the biggest difference is that React Native requires you to
define style in Javascript rather than CSS (although it does use property
names that are the same as in CSS).

If you run into problems with React or React Native, ask your coaches or come
to the App Lab, a lab I run teaching students how to build web and mobile apps.
We specialize in these technologies, and we have myself and students ready to
help. See the App Lab website [3] for more information.

[1] https://superuser.com/q/231080
[2] https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe/
[3] https://applab.unc.edu

