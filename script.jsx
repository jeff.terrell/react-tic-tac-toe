// This code assumes that React and ReactDOM have been made available already.

const newGameState = {
  squares: Array(9).fill(null),
  xIsNext: true,
}

const whoseTurn = state => state.xIsNext ? 'X' : 'O'

const onMove = (state, setState, i) => {
  let newSquares = state.squares.slice()
  const turn = whoseTurn(state)
  if (state.squares[i] || winner(state.squares)) return null
  newSquares[i] = turn
  setState({
    squares: newSquares,
    xIsNext: !state.xIsNext,
  })
}

const App = () => {
  const [state, setState] = React.useState(newGameState)
  return (
    <div>
      <Board squares={state.squares}
             onMove={(i) => onMove(state, setState, i)} />
      <Status turn={whoseTurn(state)}
              winner={winner(state.squares)}
              onNewGame={() => setState(newGameState)} />
    </div>
  )
}

const Board = ({squares, onMove}) => {
  return (
    <div className='board'>
      <Row squares={squares} startIndex={0} onMove={onMove} />
      <Row squares={squares} startIndex={3} onMove={onMove} />
      <Row squares={squares} startIndex={6} onMove={onMove} />
    </div>
  )
}

const Row = ({squares, startIndex, onMove}) => {
  return (
    <div className='row'>
      <Square label={squares[startIndex]}
              onClick={() => onMove(startIndex)} />
      <Square label={squares[startIndex + 1]}
              onClick={() => onMove(startIndex + 1)} />
      <Square label={squares[startIndex + 2]}
              onClick={() => onMove(startIndex + 2)} />
    </div>
  )
}

const Square = ({label, onClick}) => {
  return (
    <button className='square' onClick={onClick}>{label}</button>
  )
}

const Status = ({turn, winner, onNewGame}) => {
  const text = winner === null ? 'Tie game :-/'
        : winner !== undefined ? winner + ' wins!'
        : turn + "'s turn"

  React.useEffect(() => {
    document.title = text + ' | Tic-tac-toe'
  }, [text])

  return (
    <div className='status'>
      {text}
      <br />
      <button onClick={onNewGame}>Start new game</button>
    </div>
  )
}

const winner = squares => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i]
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a]
    }
  }
  if (squares.indexOf(null) === -1) return null // tie game
  return undefined
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
